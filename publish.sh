RUNNING_CONTAINERS=$(docker ps -q)
if [ "$RUNNING_CONTAINERS" ]
then
    echo "Stopping all containers..."
    docker kill $RUNNING_CONTAINERS &>/dev/null
fi

echo "Starting containers:"
CLIENT_NAME=$(node -p "require('./packages/client/package.json').name" 2> /dev/null)
CLIENT_VERSION=$(node -p "require('./packages/client/package.json').version" 2> /dev/null)
SERVER_NAME=$(node -p "require('./packages/server/package.json').name" 2> /dev/null)
SERVER_VERSION=$(node -p "require('./packages/server/package.json').version" 2> /dev/null)
echo "$CLIENT_NAME:$CLIENT_VERSION"
docker run -d -p 3001:80 tkostus/$CLIENT_NAME:$CLIENT_VERSION &>/dev/null
echo "$SERVER_NAME:$SERVER_VERSION"
docker run -d -p 5000:5000 tkostus/$SERVER_NAME:$SERVER_VERSION &>/dev/null