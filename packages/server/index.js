const polka = require('polka')
const { name, version } = require('./package.json')
var cors = require('cors')

polka()
  .use(cors())
  .get('/ping', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({ name, version: `v${version}` }))
  })
  .listen(5000, err => {
    if (err) throw err
    console.log(`> Running on localhost:5000`)
  })
