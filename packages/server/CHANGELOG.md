# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.3.6](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-server@0.3.5...clienta-appa-server@0.3.6) (2019-04-20)

**Note:** Version bump only for package clienta-appa-server





## [0.3.4](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-server@0.3.3...clienta-appa-server@0.3.4) (2019-03-31)

**Note:** Version bump only for package clienta-appa-server





# 0.3.0 (2019-03-30)


### Features

* Added CRA on the client ([2fbd438](https://gitlab.com/tkostus/monorepo/commit/2fbd438))
* Added server ([4598e55](https://gitlab.com/tkostus/monorepo/commit/4598e55))





## [0.2.2](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-server@0.2.1...clientA-appA-server@0.2.2) (2019-03-30)

**Note:** Version bump only for package clientA-appA-server





## [0.2.1](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-server@0.2.0...clientA-appA-server@0.2.1) (2019-03-30)

**Note:** Version bump only for package clientA-appA-server





# [0.2.0](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-server@0.1.0...clientA-appA-server@0.2.0) (2019-03-30)


### Features

* Added CRA on the client ([2fbd438](https://gitlab.com/tkostus/monorepo/commit/2fbd438))





# 0.1.0 (2019-03-28)


### Features

* Added server ([4598e55](https://gitlab.com/tkostus/monorepo/commit/4598e55))
