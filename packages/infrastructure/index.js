'use strict'
const pulumi = require('@pulumi/pulumi')
const azure = require('@pulumi/azure')
const k8s = require('@pulumi/kubernetes')

const name = 'at-tom-test'
const name2 = 'attomtest'

const REGISTRY_NAME = 'tkostus'

const {
  name: clientName,
  version: clientVersion
} = require('../client/package.json')

const clientImage = `${REGISTRY_NAME}/${clientName}:${clientVersion}`

const {
  name: serverName,
  version: serverVersion
} = require('../server/package.json')

const serverImage = `${REGISTRY_NAME}/${serverName}:${serverVersion}`

// Parse and export configuration variables for this stack.
const config = new pulumi.Config()
const password = config.require('password')
const location = config.get('location') || 'North Europe'
const nodeCount = config.getNumber('nodeCount') || 2
const nodeSize = config.get('nodeSize') || 'Standard_B2s'
// const sshPublicKey = config.require('sshPublicKey')
const resourceGroup = new azure.core.ResourceGroup(name, { location })

// Create the AD service principal for the K8s cluster.
const adApp = new azure.ad.Application(`${name}-aks`)
const adSp = new azure.ad.ServicePrincipal(`${name}-aks-sp`, {
  applicationId: adApp.applicationId
})
const adSpPassword = new azure.ad.ServicePrincipalPassword(
  `${name}-aks-sp-pass`,
  {
    servicePrincipalId: adSp.id,
    value: password,
    endDate: '2099-01-01T00:00:00Z'
  }
)

const k8sCluster = new azure.containerservice.KubernetesCluster(`${name}-aks`, {
  resourceGroupName: resourceGroup.name,
  location,
  servicePrincipal: {
    clientId: adApp.applicationId,
    clientSecret: adSpPassword.value
  },
  agentPoolProfile: {
    name: name2,
    count: nodeCount,
    vmSize: nodeSize
  },
  dnsPrefix: `${pulumi.getStack()}-kube`
})

// Expose a K8s provider instance using our custom cluster instance.
const k8sProvider = new k8s.Provider(name, {
  kubeconfig: k8sCluster.kubeConfigRaw
})

// Frontend
let feLabels = { app: clientName }
let feDeployment = new k8s.apps.v1.Deployment(
  clientName,
  {
    spec: {
      selector: { matchLabels: feLabels },
      replicas: config.getNumber('replicas') || 1,
      template: {
        metadata: { labels: feLabels },
        spec: {
          containers: [
            {
              name: clientName,
              image: clientImage,
              ports: [{ containerPort: 80 }]
            }
          ]
        }
      }
    }
  },
  { provider: k8sProvider }
)

// Allocate an IP
const feService = new k8s.core.v1.Service(
  clientName,
  {
    metadata: { labels: feDeployment.spec.template.metadata.labels },
    spec: {
      type: 'LoadBalancer',
      ports: [{ port: 80, targetPort: 80, protocol: 'TCP' }],
      selector: feLabels
    }
  },
  { provider: k8sProvider }
)

let beLabels = { app: serverName }
let beDeployment = new k8s.apps.v1.Deployment(
  serverName,
  {
    spec: {
      selector: { matchLabels: beLabels },
      replicas: config.getNumber('replicas') || 1,
      template: {
        metadata: { labels: beLabels },
        spec: {
          containers: [
            {
              name: serverName,
              image: serverImage,
              ports: [{ containerPort: 80 }]
            }
          ]
        }
      }
    }
  },
  { provider: k8sProvider }
)

// Allocate an IP
const beService = new k8s.core.v1.Service(
  serverName,
  {
    metadata: { labels: beDeployment.spec.template.metadata.labels },
    spec: {
      type: 'LoadBalancer',
      ports: [{ port: 80, targetPort: 5000, protocol: 'TCP' }],
      selector: beLabels
    }
  },
  { provider: k8sProvider }
)

// const sqlServer = new azure.sql.SqlServer(`${name}-sql`, {
//   administratorLogin: 'sqladministrator',
//   administratorLoginPassword: password,
//   location,
//   name: `${name}-sql`,
//   resourceGroupName: resourceGroup.name,
//   version: '12.0'
// })
// const sqlDatabase = new azure.sql.Database(`${name}-db`, {
//   location,
//   name: `${name}-db`,
//   resourceGroupName: resourceGroup.name,
//   serverName: sqlServer.name,

// })
