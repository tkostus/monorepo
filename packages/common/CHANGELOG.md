# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-common@0.4.0...clienta-appa-common@0.5.0) (2019-04-20)


### Features

* **AppName:** Changed App Name ([18508e5](https://gitlab.com/tkostus/monorepo/commit/18508e5))





# [0.4.0](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-common@0.3.2...clienta-appa-common@0.4.0) (2019-03-31)


### Features

* New appName ([9fc1510](https://gitlab.com/tkostus/monorepo/commit/9fc1510))





# 0.3.0 (2019-03-30)


### Bug Fixes

* Fix names in package.json ([ba69975](https://gitlab.com/tkostus/monorepo/commit/ba69975))


### Features

* Added server ([4598e55](https://gitlab.com/tkostus/monorepo/commit/4598e55))
* Changed appName to 'MonoRepo' ([5733296](https://gitlab.com/tkostus/monorepo/commit/5733296))





# [0.2.0](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-common@0.1.0...clientA-appA-common@0.2.0) (2019-03-30)


### Features

* Changed appName to 'MonoRepo' ([5733296](https://gitlab.com/tkostus/monorepo/commit/5733296))





# 0.1.0 (2019-03-28)


### Features

* Added server ([4598e55](https://gitlab.com/tkostus/monorepo/commit/4598e55))
