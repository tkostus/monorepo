import React, { useState, useEffect } from 'react'
import './App.css'
import axios from 'axios'
import { appName } from 'clienta-appa-common'
import { API_URL } from './constants'

const App = () => {
  const [state, setState] = useState({})
  useEffect(() => {
    const fetchData = async () => {
      const pingData = (await axios.get(`${API_URL}/ping`)).data
      setState(pingData)
    }
    fetchData()
  }, [])
  return (
    <div className='App'>
      <header className='App-header'>
        <p>
          {appName}. Client version: v{process.env.REACT_APP_VERSION || 'N/A'},
          Server version:
          {` ${(state && state.version) || 'N/A'}`}
        </p>
      </header>
    </div>
  )
}

export default App
