# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.14](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-client@0.5.13...clienta-appa-client@0.5.14) (2019-04-20)

**Note:** Version bump only for package clienta-appa-client





## [0.5.13](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-client@0.5.12...clienta-appa-client@0.5.13) (2019-04-20)

**Note:** Version bump only for package clienta-appa-client





## [0.5.7](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-client@0.5.6...clienta-appa-client@0.5.7) (2019-03-31)

**Note:** Version bump only for package clienta-appa-client





## [0.5.6](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-client@0.5.5...clienta-appa-client@0.5.6) (2019-03-31)

**Note:** Version bump only for package clienta-appa-client





# [0.5.0](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-client@0.4.1...clienta-appa-client@0.5.0) (2019-03-30)


### Features

* appName sourced from common package ([589cc56](https://gitlab.com/tkostus/monorepo/commit/589cc56))





## [0.4.1](https://gitlab.com/tkostus/monorepo/compare/clienta-appa-client@0.4.0...clienta-appa-client@0.4.1) (2019-03-30)

**Note:** Version bump only for package clienta-appa-client





# 0.4.0 (2019-03-30)


### Bug Fixes

* Fixed typo ([50d866b](https://gitlab.com/tkostus/monorepo/commit/50d866b))


### Features

* Added CRA on the client ([2fbd438](https://gitlab.com/tkostus/monorepo/commit/2fbd438))
* Added server ([4598e55](https://gitlab.com/tkostus/monorepo/commit/4598e55))
* App Name & Version info ([f31567d](https://gitlab.com/tkostus/monorepo/commit/f31567d))





## [0.3.1](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-client@0.3.0...clientA-appA-client@0.3.1) (2019-03-30)


### Bug Fixes

* Fixed typo ([50d866b](https://gitlab.com/tkostus/monorepo/commit/50d866b))





# [0.3.0](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-client@0.2.1...clientA-appA-client@0.3.0) (2019-03-30)


### Features

* App Name & Version info ([f31567d](https://gitlab.com/tkostus/monorepo/commit/f31567d))





## [0.2.1](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-client@0.2.0...clientA-appA-client@0.2.1) (2019-03-30)

**Note:** Version bump only for package clientA-appA-client





# [0.2.0](https://gitlab.com/tkostus/monorepo/compare/clientA-appA-client@0.1.0...clientA-appA-client@0.2.0) (2019-03-30)


### Features

* Added CRA on the client ([2fbd438](https://gitlab.com/tkostus/monorepo/commit/2fbd438))





# 0.1.0 (2019-03-28)


### Features

* Added server ([4598e55](https://gitlab.com/tkostus/monorepo/commit/4598e55))
